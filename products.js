var products = [
  {
    "name": "Cupcakes",
    "description": "Tasty cupcakes.",
    "image": "images/main/products-main/cupcake-main.png",
    "maxPrice": 18,
    "minPrice": 8
  },
  {
    "name": "Cakes",
    "description": "Tasty cakes.",
    "image": "images/main/products-main/cake-main.png",
    "maxPrice": 18,
    "minPrice": 8
  },
  {
    "name": "CakePops",
    "description": "Tasty cakepops.",
    "image": "images/main/products-main/cakepop-main.png",
    "maxPrice": 18,
    "minPrice": 8
  }
]

module.exports = products;