function FillingCollection () {
  this.subscribers = {
    'fetching': [],
    'fetched': [],
    'sync': [],
    'syncing': [],
  }
  //method
  this.fetchData = function (callback) {
    var data;
    this.trigger('fetching');
    //sending request to server
    ajaxSender({
      method: 'GET',
      url: '/fillings',
      context: this,
      callback: function (response) {
        data = JSON.parse(response);
        this.handleData(data);
        this.trigger('fetched');
      }
    });
  }
  this.deleteModel = function (eventData) {
    var modelIndex = _.findIndex(this.models, function (model) {
      return model.get('name') ===eventData.name;
    });
    this.models.splice(modelIndex, 1);
  }

  this.handleData = function (newData) {
    this.models = newData.map(function (filling) { 
      var model = new FillingModel(filling);
      
      model.on('delete', this.deleteModel, this);

      return model;
    }, this)};
}

FillingCollection.prototype = new BaseModel();