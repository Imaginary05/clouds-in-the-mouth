function ProductCollection () {
  this.subscribers = {
    'fetching': [],
    'fetched': [],
    'sync': [],
    'syncing': [],
  };
  //method
  this.fetchData = function (callback) {
    var data;
    this.trigger('fetching');
    //sending request to server
    ajaxSender({
      method: 'GET',
      url: '/products',
      context: this,
      callback: function (response) {
        data = JSON.parse(response);
        this.handleData(data);
        this.trigger('fetched');
      }
    });
  }
  this.deleteModel = function (eventData) {
    var modelIndex = _.findIndex(this.models, function (model) {
      return model.get('name') === eventData.name;
    });
    this.models.splice(modelIndex, 1);
    console.log(this.models);
  }
  this.handleData = function (newData) {
    this.models = newData.map(function (product) { 
      var model = new ProductModel(product);
      
        model.on('delete', this.deleteModel, this);

      return model;
    }, this);
    
  }
}

ProductCollection.prototype = new BaseModel();