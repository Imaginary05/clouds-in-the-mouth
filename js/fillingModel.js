function FillingModel (options) {
  var data = {};
  if (options) {
    data = {
    name: options.name,
    description: options.description,
    image: options.image
    }
  }
    

  this.subscribers = {
    'delete': [],
    'deleted failed': [],
    'updated': [],
    'updated failed': [],
    'syncing': [],
    'sync': [],
    'saved': [],
    'saved failed': []
  }

  this.get = function (key) {
    return data[key];
  }
  this.getData = function () {
    return {
      name: data.name,
      description: data.description,
      image: data.image
    }
  }
  this.set = function (key, value) {
    data[key] = value;

    return this;
  }
  this.update = function (newData) {
    data.name = newData.name;
    data.description = newData.description;
    this.trigger('syncing');
    $.ajax({
      method: 'PUT',
      url: '/fillings/' + data.name,
      data: this.getData(),
      context: this,
      //if updated was ok - trigger updated
      success: function () {
        this.trigger('updated');
      },
      //if updated was not ok - trigger failed
      error: function () {
        this.trigger('updated failed');
      },
      //when request is back - trigger sync 
      complete: function () {
        this.trigger('sync');
      }
    });
  }


  this.delete = function (callback) {
    $.ajax({
      method: 'DELETE',
      url: '/fillings/' + data.name,
      context: this,
      success: function () {
        this.trigger('delete', {name: data.name});
      },
      error: function () {
        this.trigger('delete failed', {name: data.name});
      },
      complete: function () {
        this.trigger('sync');
      }
    });
  }
}

FillingModel.prototype = new BaseModel();