function ProductCollectionController () {
  var collection = new ProductCollection();

  var $element = $('#productSectionSell');

  this.init = function () {
    collection.on('syncing', this.toggleBusy.bind(this, true, $element), this);
    collection.on('sync', this.toggleBusy.bind(this, false, $element));
    collection.on('fetched', this.render.bind(this, ProductController, collection, $element));
    collection.fetchData();
    return this;
  };
  this.clean = function () {
    collection.off('syncing');
    collection.off('sync');
    collection.off('fetched');
  };
}

ProductCollectionController.prototype = new BaseCollectionController();