function ajaxSender (options) {
  var request = new XMLHttpRequest();
    request.open(options.method, options.url, true);
    request.onreadystatechange = function () {
      if (this.readyState === 4) {
        options.callback.call(options.context, this.responseText);
      }
    }

    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(options.body);
}