//base model with observer implementation
function BaseModel () {
  this.on = function (event, callback, context) {
    this.subscribers[event].push({
      callback: callback,
      context: context
    });
  };
  this.trigger = function (event, data) {
    this.subscribers[event].forEach(function (info) {
      info.callback.call(info.context, data);
    });
  };
  this.off = function (event) {
    this.subscribers[event] = [];
  }
}