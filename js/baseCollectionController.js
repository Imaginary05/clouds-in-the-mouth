function BaseCollectionController () {
  this.render = function (Controller, collection, $element) {
    var html = collection.models.reduce(function ($fragment, product) {
      var controller = new Controller(product);
      $fragment.append(controller.render());
      return $fragment;
    },
    $(document.createDocumentFragment()));
    $element.append(html);
  };

  this.toggleBusy = function (isBusy, $element) {
    $element.toggleClass('is-loading', isBusy);
  };

  this.renderOne = function (Controller, model) {
    var controller = new Controller(model);
    $element.append(controller.render());
  };
}