var ProductController = (function () {

  return function (model) {

    var $el = $('<div>');
    model.on('delete', function () {
      $el.remove();
    });
    model.on('syncing', function () {
      $el.addClass('is-loading');
    });
    model.on('sync', function () {
      $el.removeClass('is-loading');
    });
    model.on('updated', function () {
      this.render();
    }, this);
    model.on('updated failed', function () {
      $el.find('.js-serverError').removeClass('is-shown');
    });
    model.on('deleted failed', function () {
      $el.find('.js-serverError').removeClass('is-shown');
    });

    this.render = function () {
      var productSection = productView({
        productName: model.get('name'),
        productDescription: model.get('description'),
        imgSrc: model.get('image')
      });

      $el.html(productSection);
      $el.addClass('productContainer');
      return $el;
    }

    function deleteProduct (event) {
      event.stopPropagation();

      model.delete();
    }

    function updateProduct () {
      //finding inputs by id
      //
      var $descriptionInput = $el.find('#productDescription');
      var $nameInput = $el.find('#productName');
      //updating model,
      //getting data to update from inputs
      //passing render method as a callback
      model.update({
        name: $nameInput.val(),
        description: $descriptionInput.val()
      });
    }
    this.renderEditView = function () {
      var content = productEditView({
        productName: model.get('name'),
        productDescription: model.get('description')
      });

      $el.html(content);


      return $el;
    }

    function showSection () {
      var sectionController = new ProductSectionController({model:model});
      sectionController.render();
    }


    $el.on('click', '.js-submit', function () {
      updateProduct.call(this);
    }.bind(this));
    $el.on('click', '.js-delete', function () {
      deleteProduct.call(this, event);
    }.bind(this));
    $el.on('click', '.js-edit', function () {
      this.renderEditView.call(this);
    }.bind(this));
    $el.on('click', function () {
      showSection();
    })
  }
})();