function FillingCollectionController () {
  var collection = new FillingCollection();

  var $element = $('#fillingSectionSell');

  this.init = function () {
    collection.on('syncing', this.toggleBusy.bind(this, true, $element), this);
    collection.on('sync', this.toggleBusy.bind(this, false, $element));
    collection.on('fetched', this.render.bind(this, FillingController, collection, $element));
    collection.fetchData();
   return this;
  };
  this.clean = function () {
    collection.off('syncing');
    collection.off('sync');
    collection.off('fetched');
  };
}

FillingCollectionController.prototype = new BaseCollectionController();