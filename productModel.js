function ProductModel (options) {
  var data = {
    name: options.name,
    description: options.description,
    image: options.image
  }

  this.subscribers = {
    delete: []
  }

  this.get = function (key) {
    return data[key];
  }
  this.getData = function () {
    return {
      name: data.name,
      description: data.description,
      image: data.image
    }
  }
  this.set = function (key, value) {
    data[key] = value;

    return this;
  }
  this.on = function (event, callback, context) {
     //event - name of the event: 'delete'
    this.subscribers[event].push({
        //we put object which describes callback
        //in array of callbacks for current 'event'
      callback: callback,//what to invoke
      context: context// and where to invoke
    });
  }
  this.trigger = function (event, data) {
      //we find a list of callbacks in subscribers object
    this.subscribers[event].forEach(function (info) {
        //and invoke it with required context
      info.callback.call(info.context, data);
    });
  }
  this.delete = function (callback) {
    ajaxSender({
      method: 'DELETE',
      url: '/products/' + data.name,
      context: this,
      callback: function () {
          //triggering delete event
        this.trigger('delete', {name: data.name});
      }
    });
  }
}