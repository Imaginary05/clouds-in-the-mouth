var express = require("express");
var bodyParser = require("body-parser");
var products = require("./products.js");
var fillings = require("./fillings.js");
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.get('/', function (req, res) {
  res.sendfile('index.html');
});
app.get('/products', function (req, res) {
    res.send(JSON.stringify(products));
});
app.delete('/products/:name', function (req, res) {
  products.forEach(function (product, index, list) {
    if (product.name === req.params.name) {
      list.splice(index, 1);
    }
  });
  res.sendStatus(200);
});
app.put('/products/:name', function (req, res) {
  products.forEach(function (product, index, list) {
    if (product.name === req.params.name) {
        list[index] = req.body;
    }
  });
  setTimeout(function () {
    res.sendStatus(200);
  }, 1000);
});
app.post('/products', function (req, res) {
    products.push(req.body);
    res.sendStatus(200);
});

/*fillings part*/

app.get('/fillings', function (req, res) {
    res.send(JSON.stringify(fillings));
});
app.delete('/fillings/:name', function (req, res) {
  fillings.forEach(function (filling, index, list) {
    if (filling.name === req.params.name) {
      list.splice(index, 1);
    }
  });
  res.sendStatus(200);
});
app.put('/fillings/:name', function (req, res) {
  fillings.forEach(function (filling, index, list) {
    if (filling.name === req.params.name) {
      list[index] = req.body;
      console.log(req.body);
    }
  });
  setTimeout(function () {
    res.sendStatus(200);
  }, 1000);
});
app.post('/fillings', function (req, res) {
    fillings.push(req.body);
    res.sendStatus(200);
});

app.use('/css', express.static('css'));
app.use('/images', express.static('images'));
app.use('/js', express.static('js'));

app.listen(8080, function () {
  console.log('Listening on 8080');
});