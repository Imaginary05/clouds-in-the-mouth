var fillings = [
  {
    "name": "Cherry-yogurt",
    "description": "Tasty-one.",
    "image": "images/main/fillings-main/cherry-yogurt-main.png"
  },
  {
    "name": "Tasty-one",
    "description": "Tasty-one.",
    "image": "images/main/fillings-main/Tasty-one-main.png"
  },
  {
    "name": "Homemade",
    "description": "Tasty-one.",
    "image": "images/main/fillings-main/Homemade-main.png"
  },
  {
    "name": "Sour-cream",
    "description": "Tasty-one.",
    "image": "images/main/fillings-main/Sour-cream-main.png"
  },
  {
    "name": "Wild-berry",
    "description": "Tasty-one.",
    "image": "images/main/fillings-main/Wild-berry-main.png"
  },
  {
    "name": "Truffle",
    "description": "Tasty-one.",
    "image": "images/main/fillings-main/Truffle-main.png"
  }
]

module.exports = fillings;